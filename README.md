# Process

1. Write a Doker file
2. Write a Compose file
3. Run

## Write a Docker file

> Dockerfile

```yml
# Everything on this line is a comment

# Node Official version 6.4 minimum
FROM node:6.4

# npm ENV LOG_LEVEL
ENV NPM_CONFIG_LOGLEVEL warn

# Define work directory
WORKDIR /api/

# use nodemon for development
RUN npm install --global nodemon

# Copy npm-shrinkwrap.json
COPY ./api/npm-shrinkwrap.json .

# Explicit
RUN npm install

COPY ./api/. . 

# Expose PORT
EXPOSE 8000

# Run commands
CMD ["npm","start"]

```
-t TAG name image
> docker build -t oc-api .

:1.0.0 versioning
--no-cache Do not use cache when building the image
--force-rm Always remove intermediate containers
> docker build  -t oc-api:1.0.0 . --no-cache --force-rm

-it attach teminal session
-p PORT forwarding
> docker run -it -p 8000:8000 oc-api:1.0.0 npm run start

-v $(pwd)/api:/api mount loal volume in image
> docker run -it -p 8000:8000 -v $(pwd)/api:/api oc-api:1.0.0 npm run dev


## Docker To Open
Grab the cluster ip address provided for openshift internal registry

> 172.30.17.124:5000/orsys-park/oc-api
> docker tag oc-api:1.0.0 172.30.17.124:5000/orsys-park/oc-api 
> docker push 172.30.17.124:5000/orsys-park/oc-api 
> oc new-app orsys-park/oc-api:1.0.0  --name=oc-api