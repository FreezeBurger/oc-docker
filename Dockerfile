# Everything on this line is a comment

# Node Official version 6.4 minimum
FROM node:6.4

# npm ENV LOG_LEVEL
ENV NPM_CONFIG_LOGLEVEL warn

# Define work directory
WORKDIR /api/

COPY ./api/ . 

EXPOSE 8000

# Run commands
ENTRYPOINT ["npm","start"]
